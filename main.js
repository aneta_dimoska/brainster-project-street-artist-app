
let btnChooseArtistLandingPage = document.querySelector('.btn-click'),
   
    dropdowndiv = document.querySelector('.dropdown-custom'),
    joinVisitorBtn = document.querySelector('.join-visitor'),
    sectionLandingPage = document.querySelector('#landingPage'),
    sectionVisitorHomePage = document.querySelector('#visitor'),
    bannerVistorHomePage = document.querySelector('.banner'),
    btnBannerVisitor = document.querySelector('.visitor-home-page-btn'),
    sliderVisitorHomePage = document.querySelector('.visitor-home-page .slider'),
    slideLeftTrackVisitorHomePage = document.querySelector('.slide-track-left'),
    slideRightTrackVisitorHomePage = document.querySelector('.slide-track-right'),
    sectionVisitorListingPage = document.querySelector('#visitorListing'),
    rowVisitorListingPage = document.querySelector('.listing-section-bg'),
    containerVisitorListingPage = document.querySelector('#visitorListing .container-fluid'),
    btnFiltersListing = document.querySelector('.visitor-listing-page .btnFilters'),
    cardItem = document.querySelector('.card-item'),
    sectionVisitorsFilters = document.querySelector('#visitorFilters'),
    formFilters = document.querySelector('#formFilters'),
    selectFilterArtist = document.querySelector('.visitor-filters #byArtistFilter'),
    selectFiltertype = document.querySelector('.form-type #selectType'),
    filterBtn = document.querySelector('.filters-btn-submit'),
    itemTitleInput = document.querySelector('#itemTitleInput'),
    minPriceInput = document.querySelector('.form-price #min-price'),
    maxPriceInput = document.querySelector('.form-price #max-price'),
    filterByTipe = document.querySelector('.form-type .select-type'),
    sectionArtistHome = document.querySelector('#artists'),
    rowArtisthome = document.querySelector('#artists .row'),
    totalItemsSold = document.querySelector('.total-sold span'),
    totalIncomeArtist = document.querySelector('.total-income span'),
    sectionArtistItemPage = document.querySelector('#artistsItems'),
    rowArtistItems = document.querySelector('#artistsItems .row'),
    addNewItemBtn = document.querySelector('#artistsItems .addNewItemDiv'),
    sectionArtistAddNewItem = document.querySelector('#artistAddNewItem'),
    rowArtistAddNewItem = document.querySelector('#artistAddNewItem .row'),
    isPublishedCheckBox = document.querySelector('#artistAddNewItem .isPublishedCheckbox'),
    formAddNewItem = document.querySelector('.addNewItemForm'),
    addNewTitle = document.querySelector('.addTitle'),
    addNewDescription = document.querySelector('.addDescription'),
    addNewType = document.querySelector('.addType'),
    addNewPrice = document.querySelector('.addPrice'),
    addNewImg = document.querySelector('.addImg'),
    btnAddItem = document.querySelector('.btn-add-new-item'),
    btnCancelNewItem = document.querySelector('.btn-cancel-item'),
    sectionAristCapturePage = document.querySelector('#artistCaptireImg'),
    rowCapturePage = document.querySelector('#artistCaptireImg .row'),
    sectionAuctionPage = document.querySelector('#auctionPage'),
    containerAuctionPage = document.querySelector('auctionPage .container-fluid'),
    rowAuctionPage = document.querySelector('#auctionPage .row'),
    liveAuctionDiv = document.querySelector('.live-auction-item'),
    modalRemove = document.querySelector('.modalRemove'),
    btnYesRemove = document.querySelector('.modal-footer .btnYes'),
    btnCancelRemove = document.querySelector('.modal-footer .btnCancel'),
    snapShotDiv = document.querySelector('.snapshot-div'),
    canvasImg = document.querySelector('#canvas'),
    videoStream = document.querySelector('#video'),
    outputImageCamera = document.querySelector('#artistCaptireImg .output'),
    cameraBtns = document.querySelector('.btnDivCamera'),
    takenPhoto = document.querySelector('.takenPhoto'),
    currentLiveBid = document.querySelector('.live-auction-item span'),
    closeFilters= document.querySelector('.fa-times'),
    priceTag,
    userLoggedLS,
    filteredCardsArr,
    publishedArr,
    currentItemObj,
    currentItemHtml,
    isPublishedItem,
    itemsFromLS,
    currentArtist,
    itemOnAuction,
    btnSendAuction,
    isVisitorMenu = true,
    isArtistMenu = true,
    isOnLiveAuction = false,
    isEditing = false,
    isArtist;
    const currentBiddingItem = {}
  
    


    const onLoad = () => {
          
            fromLocalStorage()
          
            location.hash = ''
            sectionLandingPage.classList.add('displayBlock')

            sectionArtistHome.classList.remove('displayBlock')
            sectionArtistItemPage.classList.remove('displayBlock')
            sectionArtistAddNewItem.classList.remove('displayBlock')
            sectionVisitorHomePage.classList.remove('displayBlock')
            sectionVisitorListingPage.classList.remove('displayBlock')
            sectionVisitorsFilters.classList.remove('displayBlock')
            sectionAristCapturePage.classList.remove('displayBlock')
            sectionAuctionPage.classList.remove('displayBlock')
            
    }

    const handleRoute = () => {
        let _hash = location.hash
       
        switch (_hash) {
            case '' :
            location.hash = ''
            sectionLandingPage.classList.add('displayBlock')

            sectionArtistHome.classList.remove('displayBlock')
            sectionArtistItemPage.classList.remove('displayBlock')
            sectionArtistAddNewItem.classList.remove('displayBlock')
            sectionVisitorHomePage.classList.remove('displayBlock')
            sectionVisitorListingPage.classList.remove('displayBlock')
            sectionVisitorsFilters.classList.remove('displayBlock')
            sectionAristCapturePage.classList.remove('displayBlock')
            sectionAuctionPage.classList.remove('displayBlock')

            break;

            case '#artists':
            location.hash = '#artists'
            sectionArtistHome.classList.add('displayBlock')
            sectionLandingPage.classList.remove('displayBlock')
            sectionArtistItemPage.classList.remove('displayBlock')
            sectionArtistAddNewItem.classList.remove('displayBlock')
            sectionVisitorHomePage.classList.remove('displayBlock')
            sectionVisitorListingPage.classList.remove('displayBlock')
            sectionVisitorsFilters.classList.remove('displayBlock')
            sectionAristCapturePage.classList.remove('displayBlock')
            sectionAuctionPage.classList.remove('displayBlock')

            artistMenuNav(rowArtisthome, 'home')
            chartFn()


            liveAuctionDiv.addEventListener('click', () => {
                let liveAuctionFromLS = localStorage.getItem('isOnLiveAuction')
                
                if (liveAuctionFromLS) {
                    location.hash = '#auction'
                    } else {
                        console.log('No item on auction')
                    }
        
            })
          

            break;

            case '#artists/items': 
            sectionArtistItemPage.classList.add('displayBlock')
    
            sectionArtistHome.classList.remove('displayBlock')
            sectionLandingPage.classList.remove('displayBlock')
            sectionArtistAddNewItem.classList.remove('displayBlock')
            sectionVisitorHomePage.classList.remove('displayBlock')
            sectionVisitorListingPage.classList.remove('displayBlock')
            sectionVisitorsFilters.classList.remove('displayBlock')
            sectionAristCapturePage.classList.remove('displayBlock')
            sectionAuctionPage.classList.remove('displayBlock')

            artistMenuNav(rowArtistItems, 'items')
            fromLocalStorage()
     
            break;

            case '#auction':
            sectionAuctionPage.classList.add('displayBlock')
    
            sectionArtistHome.classList.remove('displayBlock')
            sectionLandingPage.classList.remove('displayBlock')
            sectionArtistAddNewItem.classList.remove('displayBlock')
            sectionVisitorHomePage.classList.remove('displayBlock')
            sectionVisitorListingPage.classList.remove('displayBlock')
            sectionVisitorsFilters.classList.remove('displayBlock')
            sectionAristCapturePage.classList.remove('displayBlock')
            sectionArtistItemPage.classList.remove('displayBlock')

            auctionPageFn()

            break;

            case '#addNewItem':
            sectionArtistAddNewItem.classList.add('displayBlock')

            sectionArtistItemPage.classList.remove('displayBlock')
            sectionArtistHome.classList.remove('displayBlock')
            sectionLandingPage.classList.remove('displayBlock')
            sectionVisitorHomePage.classList.remove('displayBlock')
            sectionVisitorListingPage.classList.remove('displayBlock')
            sectionVisitorsFilters.classList.remove('displayBlock')
            sectionAristCapturePage.classList.remove('displayBlock')
            sectionAuctionPage.classList.remove('displayBlock')

            artistMenuNav(rowArtistAddNewItem)
            optionSelect(addNewType) 
            break;

            case '#visitor':
                sectionVisitorHomePage.classList.add('displayBlock')

                sectionArtistAddNewItem.classList.remove('displayBlock')
                sectionArtistItemPage.classList.remove('displayBlock')
                sectionArtistHome.classList.remove('displayBlock')
                sectionLandingPage.classList.remove('displayBlock')
                sectionVisitorListingPage.classList.remove('displayBlock')
                sectionVisitorsFilters.classList.remove('displayBlock')
                sectionAristCapturePage.classList.remove('displayBlock')
                sectionAuctionPage.classList.remove('displayBlock')

            break;

            case '#visitor/listing':
                sectionVisitorListingPage.classList.add('displayBlock')

                sectionVisitorHomePage.classList.remove('displayBlock')
                sectionArtistAddNewItem.classList.remove('displayBlock')
                sectionArtistItemPage.classList.remove('displayBlock')
                sectionArtistHome.classList.remove('displayBlock')
                sectionLandingPage.classList.remove('displayBlock')
                sectionVisitorsFilters.classList.remove('displayBlock')
                sectionAristCapturePage.classList.remove('displayBlock')
                sectionAuctionPage.classList.remove('displayBlock')
                visitorListingPageFN()


                
            break;

            case '#visitor/filters':
                sectionVisitorsFilters.classList.add('displayBlock')
                
                sectionVisitorListingPage.classList.remove('displayBlock')
                sectionVisitorHomePage.classList.remove('displayBlock')
                sectionArtistAddNewItem.classList.remove('displayBlock')
                sectionArtistItemPage.classList.remove('displayBlock')
                sectionArtistHome.classList.remove('displayBlock')
                sectionLandingPage.classList.remove('displayBlock')
                sectionAristCapturePage.classList.remove('displayBlock')
                sectionAuctionPage.classList.remove('displayBlock')

                visitorFiltersPageFN()
                closeFilters.addEventListener('click', () => {
                    location.hash = '#visitor/listing'
                    isVisitorMenu = false
                })
                
                

                break;
               


            case '#capture/image':
                sectionAristCapturePage.classList.add('displayBlock')

                sectionVisitorsFilters.classList.remove('displayBlock')
                sectionVisitorListingPage.classList.remove('displayBlock')
                sectionVisitorHomePage.classList.remove('displayBlock')
                sectionArtistAddNewItem.classList.remove('displayBlock')
                sectionArtistItemPage.classList.remove('displayBlock')
                sectionArtistHome.classList.remove('displayBlock')
                sectionLandingPage.classList.remove('displayBlock')
                sectionAuctionPage.classList.remove('displayBlock')
        
                artistMenuNav(rowCapturePage)
                initCaptureImagePage()


        }
    }
    

const onChooseArtistBtn = (e) => {
    dropdowndiv.innerHTML = ''
    isArtist = true
    localStorage.setItem('isArtist', 'true')
  
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((res) => res.json())
    .then((data) => {
    
        for (let i = 0; i < data.length; i++) {

           const dropItem = document.createElement('a'); // dropItem contains the name of the Artist taken from API
           dropItem.classList.add('dropdown-item');
           dropItem.innerText = data[i].name;
           dropdowndiv.appendChild(dropItem)
           dropItem.addEventListener('click', () => {
               currentArtist = dropItem.innerText
               console.log(currentArtist)
               location.hash = '#artists'
               // displaying menu on Artist Pages
               if (isArtistMenu) {
                sectionArtistHome.prepend(artistMenu(dropItem.innerText))
                sectionArtistItemPage.prepend(artistMenu(dropItem.innerText))
                sectionAuctionPage.prepend(artistMenu(dropItem.innerText))
                sectionArtistAddNewItem.prepend(artistMenu(dropItem.innerText))
                sectionAristCapturePage.prepend(artistMenu(dropItem.innerText))

               }

               let totalArtistIncome = 0; // to accumulate the total income

               items.forEach((eachItem, idx) => (eachItem.id = idx))

               const filteredDataItemsArr = items.filter((eachArtistItem) => {
                   return eachArtistItem.artist === dropItem.innerText
               }) // to create new array filtered by the artist name chosen on landing page

               const soldItemsArr = filteredDataItemsArr.filter((eachArtistItem) => {
                totalArtistIncome += eachArtistItem.priceSold
                   return eachArtistItem.dateSold
               }) // to create new array from the filtered one (filteredDataItemsArr) by artist name to get how many pics are sold from that particular artist
               console.log(soldItemsArr)
               
               totalItemsSold.innerText = `${soldItemsArr.length} / ${filteredDataItemsArr.length}`
               totalIncomeArtist.innerText = `$${totalArtistIncome}`
               

               // to render the cards from one selected artist in Artist Items Page
           
                filteredDataItemsArr.forEach(eachSoldItem => {
            
                   let cardArtistItem = cardArtistItemFn(eachSoldItem.id, eachSoldItem.image, eachSoldItem.title, eachSoldItem.price, eachSoldItem.description, eachSoldItem.isPublished)
                //    isPublishedItem = eachSoldItem.isPublished
                   rowArtistItems.appendChild(cardArtistItem)

                   console.log(cardArtistItem)
               })
               console.log(filteredDataItemsArr)
               let allSendAuctionBtns = document.querySelectorAll('.sendAuctionBtn')
               let itemAuctionLS = JSON.parse(localStorage.getItem('itemOnAuction'))  


              // Send to Auction
              function disableSendAuctionBtn() {
                if (itemAuctionLS) {
                  allSendAuctionBtns.forEach(eachBtn => {
                  eachBtn.setAttribute('disabled', 'true')
                  })
                }

              }
              disableSendAuctionBtn()

              document.addEventListener('click', (e) => {
                 
                  if (e.target.classList.contains('sendAuctionBtn')){
                    currentItemHtml = e.target.parentElement.parentElement.parentElement
                    currentItemObj = items[currentItemHtml.id]
                    itemOnAuction = currentItemObj
                    console.log(itemOnAuction)
                    localStorage.setItem('itemOnAuction', JSON.stringify(itemOnAuction))
                    allSendAuctionBtns.forEach(eachBtn => {
                        eachBtn.setAttribute('disabled', 'true')
                        }) 
                }

              })
      
               // ADD NEW ITEM / Cancel New Item
               btnAddItem.addEventListener('click', onAddNewItem)
               btnCancelNewItem.addEventListener('click', onCancelItem)

               function onAddNewItem(e) {
                e.preventDefault()
                if (isEditing) {
                    // update the object data
                    currentItemObj.title = addNewTitle.value
                    currentItemObj.description = addNewDescription.value
                    currentItemObj.type = addNewType.value
                    currentItemObj.price = addNewPrice.value
                    currentItemObj.image = addNewImg.value

                    // update html form
                    const currentItemID = currentItemHtml.id
                    const currentCard = document.getElementById(currentItemID)

                    const editedTitle = addNewTitle.value
                    const editedImg = addNewImg.value
                    const editedPrice = addNewPrice.value
                    const editedDescription = addNewDescription.value
                    
                    currentCard.innerHTML = ''
                 
                    cardArtistItemFn(currentItemID, editedImg, editedTitle, editedPrice, editedDescription)
                    localStorage.setItem('itemsInLS', JSON.stringify(items))

                    location.hash = '#artists/items'
                    btnAddItem.innerText = 'Add New Item'

                    isEditing = false

                }else {
                    const newItemObj = {
                        "id": new Date().valueOf(),
                        "title": addNewTitle.value ,
                        "description": addNewDescription.value,
                        "type": addNewType.value,
                        "image": addNewImg.value,
                        "price": addNewPrice.value,
                        "artist": dropItem.innerText,
                        "dateCreated": "2021-10-09T02:00:48.989Z",
                        "isPublished": true,
                        "isAuctioning": false,
                        "dateSold": "2021-10-10T02:00:48.989Z",
                        // "dateSold": new Date(),

                        "priceSold": 3000
                    }
                    
                    items.push(newItemObj)
                    localStorage.setItem('itemsInLS', JSON.stringify(items))

                    addNewTitle.value = ''
                    addNewDescription.value = ''
                    addNewType.value = ''
                    addNewPrice.value = ''
                    addNewImg.value = ''

                    location.hash = '#artists/items'
                }
                  
            }

                function onCancelItem (e) {
                e.preventDefault()

                    location.hash = '#artists/items'

                    addNewTitle.value = ''
                    addNewDescription.value = ''
                    addNewType.value = 'All'
                    addNewPrice.value = ''
                    addNewImg.value = ''

                }
                // Publish - Unpublish
                document.addEventListener('click', (e) => {
                    if (e.target.classList.contains('btnPublish')) {
                        console.log('publish cliked')
            
                        currentItemHtml = e.target.parentElement.parentElement.parentElement
                     

                        localStorage.setItem('itemsInLS', JSON.stringify(items))


                        
                    }
                })

                // EDIT mode
                document.addEventListener('click', (e) => {
                    if (e.target.classList.contains('btn-edit')) {
                        console.log('edit is clicked')
                        location.hash = '#addNewItem'
                        isEditing = true
                        currentItemHtml = e.target.parentElement.parentElement.parentElement
                        currentItemObj = items[currentItemHtml.id]
                        console.log(items)
                        console.log(currentItemHtml.id)

                        // taking already entered data
                        addNewTitle.value = currentItemObj.title
                        addNewDescription.value = currentItemObj.description
                        addNewType.value = currentItemObj.title
                        addNewPrice.value = currentItemObj.price
                        addNewImg.value = currentItemObj.image

                        btnAddItem.innerText = 'Save Updates'

                    }
                })

                // Delete item
                document.addEventListener('click', (e) => {
                     
                    if (e.target.classList.contains('btn-remove')){
                        currentItemHtml = e.target.parentElement.parentElement.parentElement
                        modalRemove.style.display = 'block'
                    }
                    if (e.target.classList.contains('btnYes')) {
                        currentItemHtml.remove()
                        modalRemove.style.display = 'none'
                        console.log(parseInt(currentItemHtml.id))

                        items.splice(parseInt(currentItemHtml.id), 1)
                        localStorage.setItem('itemsInLS', JSON.stringify(items))

                    }

                })

                btnCancelRemove.addEventListener('click', () => {
                    modalRemove.style.display = 'none'
                })

        
                // Snapshot Capture
                snapShotDiv.addEventListener('click', onCapture)
                function onCapture(e) {
                    location.hash = '#capture/image'
                }
           })
           
        }

    })
}


btnChooseArtistLandingPage.addEventListener('click', onChooseArtistBtn)

// click on JOIN as Visitor button
const onJoinVisitorBtn = () => {
    isArtist = false
    location.hash = '#visitor'
    localStorage.setItem('isArtist', 'false')
    if (isVisitorMenu) {
        sectionVisitorHomePage.prepend(visitorMenu())
    }
    sliderAnimatedFN(slideLeftTrackVisitorHomePage) 
    sliderAnimatedFN(slideRightTrackVisitorHomePage) 
}
joinVisitorBtn.addEventListener('click', onJoinVisitorBtn)

// click on Banner button (Find one now!)
btnBannerVisitor.addEventListener('click', () => {
    location.hash = '#visitor/listing'
})

// ------------------- Visitor Listing Page -------------------------
function visitorListingPageFN() {
    sectionVisitorListingPage.prepend(visitorMenu())

// filtering items -  isPublished: true
publishedArr = items.filter((item) => {
        let isPublished = true
        return item.isPublished === isPublished
    })
    
// rendering cards on Visitor Listing Page
publishedArr.forEach((item, idx) => {
    let filteredItemsArr = cardVisitorListingFN(item.id, item.image, item.artist, item.price, item.title, item.description)
    cardItem.appendChild(filteredItemsArr)
    if (idx % 2 !== 0) {
       
        filteredItemsArr.classList.add('even-card')
        priceTag.classList.add('price-span-even')
    } else {
      
        filteredItemsArr.classList.add('odd-card')
        priceTag.classList.add('price-span-odd')

    }
} )

btnFiltersListing.addEventListener('click', () => {
    location.hash = '#visitor/filters'
})
}

// ------------------- Visitor Filters ---------------------------

   function visitorFiltersPageFN() {
    if (isVisitorMenu) {
        sectionVisitorsFilters.prepend(visitorMenu())

    }
    selectArtistFilter()
    optionSelect(selectFiltertype)
    filterBtn.addEventListener('click', onFilterCards)

    function onFilterCards() {


        let title = itemTitleInput.value,
                artist = selectFilterArtist.value,
                minPrice = parseInt(minPriceInput.value),
                maxPrice = parseInt(maxPriceInput.value),
                type = filterByTipe.value
    
             
                 filteredCardsArr = publishedArr.filter(item => {
                 
                    return (title ? item.title.includes(title) : true) &&
                        (artist ? item.artist === artist : true) &&
                        (minPrice ? item.price >= minPrice : true) &&
                        (maxPrice ? item.price <= maxPrice : true) &&
                        (type ? item.type === type : true)
        
                })

                sectionVisitorListingPage.innerHTML = ''
              
                filteredCardsArr.forEach(eachCard => {
                    let filteredItemsArr = cardVisitorListingFN(eachCard.id, eachCard.image, eachCard.artist, eachCard.price, eachCard.title, eachCard.description)
                cardItem.appendChild(filteredItemsArr)
                rowVisitorListingPage.appendChild(cardItem)
                containerVisitorListingPage.appendChild(rowVisitorListingPage)
                sectionVisitorListingPage.appendChild(containerVisitorListingPage)

                })
                location.hash = '#visitor/listing'
                
                
     
    }
   }

// -------------------- AUCTION ------------------------------------

function auctionPageFn() {
    userLoggedLS = localStorage.getItem('isArtist')
    artistMenuNav(rowAuctionPage, 'auction')
    auctionContentUser()
    if (userLoggedLS === 'false') {
    auctionBackIcon()
    }
    rowAuctionPage.appendChild(auctionPage())
}


// ------------------- Artist Items Page ---------------------------
addNewItemBtn.addEventListener('click', () => {
    location.hash = '#addNewItem'
})

// ---------------- CHART -------------

function chartFn() {
    const artistChart = document.querySelector('#myChart');
const config = {
    type: 'bar',
    data: {
        labels: ['10', '20', '30', '40', '50', '60', '70', '80', '90'], 
        datasets: [{
            label: 'Amount',
            data: [5,2,12,9] 
        }],
    },
    options: {
        indexAxis: 'y',
    }
};
  const myChart = new Chart( artistChart, config );


  const minDate = new Date()
  minDate.setDate(minDate.getDate() - 7);
  const maxDate = new Date()
}
  



  window.addEventListener("hashchange", handleRoute);
  window.addEventListener("load", onLoad);

 