// create visitor menu

const visitorMenu = () => {
    const navVisitor = document.createElement('nav')

    const image = document.createElement("img")
    image.setAttribute("src", "./Photos/Logo.png")
    image.classList.add('image')
    image.addEventListener('click', () => {
        location.hash = ''
        isVisitorMenu = false
    })
    navVisitor.appendChild(image)

    const h1 = document.createElement('h1')
    h1.classList.add('mb-0')
    h1.innerText = 'Street ARTist'
    navVisitor.appendChild(h1)

    const imageMenu = document.createElement("img")
    imageMenu.setAttribute("src", "./Photos/Vector.svg")
    imageMenu.classList.add('logoRight')
    imageMenu.addEventListener('click', () => {
        location.hash = '#auction'
    })
    navVisitor.appendChild(imageMenu)

    navVisitor.classList.add('text-center', 'py-3', 'position-relative')

    return navVisitor

}

// create slider part on Visitor Home Page

function sliderAnimatedFN (slideTrack) {

    for (let i = 0; i < items.length; i++) {
        let slide = document.createElement('div')
        slide.classList.add('slide')

        let imgSlide = document.createElement('img')
        imgSlide.setAttribute("src", items[i].image)
        imgSlide.classList.add('slide-img')
        imgSlide.addEventListener('click', () => {
            location.hash = '#visitor/listing'
        })

        slide.appendChild(imgSlide)
        slideTrack.appendChild(slide)
        slideTrack.appendChild(slide)

    
    }

}

// create card on Visitor Listing Page 

function cardVisitorListingFN (id, image, artist, price, title, description) {

    
    let itemCard = document.createElement('div')
    itemCard.classList.add('mt-5')
    itemCard.setAttribute('id', id)

    let innerDiv = document.createElement('div')
    innerDiv.classList.add('px-3', 'py-2')
    
    let cardImg = document.createElement('img')
    cardImg.setAttribute("src", image)
    cardImg.classList.add('w-100', 'd-block')
    itemCard.appendChild(cardImg)

    let nameAndPrice = document.createElement('div')
    nameAndPrice.classList.add('d-flex','justify-content-between')

    let artistName = document.createElement('span')
    artistName.innerText = artist
    artistName.classList.add('font-rennie', 'font-30')

    nameAndPrice.appendChild(artistName)


    priceTag = document.createElement('span')
    priceTag.classList.add('align-self-center', 'px-2')
    priceTag.innerText = `$${price}`
    nameAndPrice.appendChild(priceTag)
    innerDiv.appendChild(nameAndPrice)

    let itemTitle = document.createElement('p')
    itemTitle.innerText = title
    itemTitle.classList.add('font-14', 'mb-0')
    innerDiv.appendChild(itemTitle)

    let cardDescription = document.createElement('p')
    cardDescription.innerText = description
    cardDescription.classList.add('font-11')
    innerDiv.appendChild(cardDescription)

    itemCard.appendChild(innerDiv)



    return itemCard
    
}
// create artist menu
const artistMenu = (_artistName) => {
    const navArtist = document.createElement('nav')

    // dropMenuArtist()

    const imageLogo = document.createElement("img")
    imageLogo.setAttribute("src", "./Photos/Logo.png")
    imageLogo.classList.add('image')
    imageLogo.addEventListener('click', () => {
        location.hash = ''
        isArtistMenu = false
    })
    navArtist.appendChild(imageLogo)

    const h1 = document.createElement('h1')
    h1.classList.add('mb-0', 'd-inline-block')
    h1.innerText = _artistName
    navArtist.appendChild(h1)

    const imageBarMenuArtist = document.createElement("img")
    imageBarMenuArtist.setAttribute("src", "./Photos/bar.png")
    imageBarMenuArtist.classList.add('artist-bar', 'clicked')
    // imageBarMenuArtist.addEventListener('click', dropMenuArtist)
    navArtist.appendChild(imageBarMenuArtist)

    navArtist.classList.add('text-center', 'py-3', 'position-relative')

    return navArtist

}


// droping menu Artist Home Page

const dropMenuArtist = () => {
    const col12 = document.createElement('div')
    col12.classList.add('col-12', 'position-relative')
    const menuArtistDropDown = document.createElement('ul')
    menuArtistDropDown.classList.add('menuBar', 'position-absolute')
    menuArtistDropDown.style.height = '230px'
  
    col12.appendChild(menuArtistDropDown)

    const liItemHome = document.createElement('li')
    liItemHome.classList.add('list-unstyled', 'home')
    liItemHome.innerText = 'Home'
    liItemHome.addEventListener('click', () => {
        console.log('home is clicked')
        location.hash = '#artists'
    } )
    menuArtistDropDown.appendChild(liItemHome)

    const liItemITEMS = document.createElement('li')
    liItemITEMS.classList.add('list-unstyled', 'items')
    liItemITEMS.innerText = 'Items'
    liItemITEMS.addEventListener('click', () => {
        console.log('Items is clicked')
        location.hash = '#artists/items'
    
    })
    menuArtistDropDown.appendChild(liItemITEMS)

    const liItemAuction = document.createElement('li')
    liItemAuction.classList.add('list-unstyled', 'auction')
    liItemAuction.innerText = 'Auction'
    liItemAuction.addEventListener('click', () => {
        console.log('Auction is clicked')
        location.hash = '#auction'
  
    } )
    menuArtistDropDown.appendChild(liItemAuction)

    return col12
}
const createdDropdown = dropMenuArtist()

// create card on Artist Items Page

const cardArtistItemFn = (_id, _image, _title, _price, _description, _isPublished ) => {
    const col12Artist = document.createElement('div')
    col12Artist.classList.add('col-12', 'px-0', 'mt-3', 'itemCardsArtist')
    col12Artist.setAttribute('id', _id)

    let itemCardArtist = document.createElement('div')
    itemCardArtist.classList.add('px-0', 'py-2', 'w-100')

    let cardImgArtist = document.createElement('img')
    cardImgArtist.setAttribute("src", _image)
    cardImgArtist.classList.add('w-100', 'd-block')
    itemCardArtist.appendChild(cardImgArtist)

    let titleDivArtist = document.createElement('div')
    titleDivArtist.classList.add('d-flex', 'justify-content-between', 'background-light', 'px-4', 'py-2')

    let titleDate = document.createElement('div')
    let itemTitleArtist = document.createElement('p')
    itemTitleArtist.classList.add('mb-0', 'item-title-style')
    itemTitleArtist.innerText = _title
    titleDate.appendChild(itemTitleArtist)

    let dateArtist = document.createElement('p')
    dateArtist.classList.add('mb-0', 'date-artist-p')
    dateArtist.innerText = '15.10.2021'
    titleDate.appendChild(dateArtist)

    let priceArtist = document.createElement('span')
    priceArtist.classList.add('align-self-center', 'item-span-price')
    priceArtist.innerText = _price

    let descParArtist = document.createElement('p')
    descParArtist.classList.add('desc-artist', 'px-4', 'py-2', 'mb-0', 'background-light')
    descParArtist.innerText = _description

    let btnBoxArtist = document.createElement('div')
    btnBoxArtist.classList.add('bg-primary-default', 'px-4', 'py-3', 'd-flex', 'justify-content-between')

    btnSendAuction = document.createElement('button')
    btnSendAuction.classList.add('btn', 'bg-primary', 'text-light', 'btn-artist', 'sendAuctionBtn')
    btnSendAuction.innerText = 'Send to Auction'
    btnBoxArtist.appendChild(btnSendAuction)

    let btnUnpublish = document.createElement('button')
    btnUnpublish.classList.add('btn', 'bg-success', 'text-light', 'btn-artist', 'btnUnpublish')
    btnUnpublish.innerText = 'Unpublish'
    btnBoxArtist.appendChild(btnUnpublish)
    btnUnpublish.addEventListener('click', () => {
        if (_isPublished === true) {
            _isPublished = false
           
            btnPublish.style.display = 'block'
            btnUnpublish.style.display = 'none'

            console.log('now is unpublished')
            console.log(_isPublished)
        }
        
    })

    let btnPublish = document.createElement('button')
    btnPublish.classList.add('btn', 'bg-info', 'text-light', 'btn-artist', 'btnPublish')
    btnPublish.innerText = 'Publish'
    btnBoxArtist.appendChild(btnPublish)
    btnPublish.addEventListener('click', () => {
        if (_isPublished === false) {
            _isPublished = true
            
            btnPublish.style.display = 'none'
            btnUnpublish.style.display = 'block'
            

            console.log('now is published')
            console.log(items.isPublished)
            }

    })

    if (_isPublished) {
        btnPublish.style.display = 'none'
    } else {
        btnUnpublish.style.display = 'none'
    }

    let btnRemove = document.createElement('button')
    btnRemove.classList.add('btn', 'bg-danger', 'text-light', 'btn-artist', 'btn-remove')
    btnRemove.innerText = 'Remove'
    btnBoxArtist.appendChild(btnRemove)

    let btnEdit = document.createElement('button')
    btnEdit.classList.add('btn', 'btn-artist', 'btn-edit')
    btnEdit.innerText = 'Edit'
    btnBoxArtist.appendChild(btnEdit)
    


    titleDivArtist.append(titleDate, priceArtist)
    itemCardArtist.append(titleDivArtist, descParArtist, btnBoxArtist)
    col12Artist.appendChild(itemCardArtist)

    return col12Artist

}

// function for navigating the dropdown menu on Artist pages
const artistMenuNav = (_sectionRow, _liClass) => {

    document.addEventListener('click', (e) => {
        if (e.target.classList.contains('clicked')) {
           
            _sectionRow.prepend(createdDropdown)
        }
        if (e.target.classList.contains(`${_liClass}`)) {
         
            _sectionRow.removeChild(createdDropdown)
        }

    })

}
// create option in form (Add New Item)
function optionSelect(_select) {
    const optionAll = document.createElement('option')
    optionAll.innerText = 'All'
    _select.prepend(optionAll)
    
    itemTypes.forEach(eachType => {
        const option = document.createElement('option')
        option.innerText = eachType
        _select.appendChild(option)
    })
}

// create options in Filters by artist 
function selectArtistFilter() {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((res) => res.json())
    .then((data) => {

        for (let i = 0; i < data.length; i++) {
            const optionArtist = document.createElement('option')
            optionArtist.innerText = data[i].name
            optionArtist.setAttribute('value', `${data[i].name}`)
            selectFilterArtist.appendChild(optionArtist)
        }
      
    }) 
}

// get from Local Storage
function fromLocalStorage() {
    itemsFromLS = JSON.parse(localStorage.getItem('itemsInLS'))
    if (itemsFromLS) {
        items = itemsFromLS.slice()
    }
}

// }

// remove display none on sections
function removeDisplayNone(_section) {
    _section.classList.remove('displayNone')
}
// auction page Visitor vs. Artist
function auctionContentUser() {
    let auctionItemFromLS = JSON.parse(localStorage.getItem('itemOnAuction'))
    userLoggedLS = localStorage.getItem('isArtist')

    if (userLoggedLS === 'true') {
        console.log(auctionItemFromLS)

        if (auctionItemFromLS) {
            isOnLiveAuction = true
            localStorage.setItem('isOnLiveAuction', 'true')
            let cardOnAuction = cardVisitorListingFN(auctionItemFromLS.id, auctionItemFromLS.image, auctionItemFromLS.artist, auctionItemFromLS.price, auctionItemFromLS.title, auctionItemFromLS.description)


            rowAuctionPage.innerHTML = ''
            rowAuctionPage.prepend(cardOnAuction)
            auctionPage()

        } else {
            alert('no item in auction')
           

        }

       
    } 
    if (userLoggedLS === 'false') {

            console.log(userLoggedLS)
            localStorage.setItem('isOnLiveAuction', 'true')
            let cardOnAuction = cardVisitorListingFN(auctionItemFromLS.id, auctionItemFromLS.image, auctionItemFromLS.artist, auctionItemFromLS.price, auctionItemFromLS.title, auctionItemFromLS.description)


            rowAuctionPage.innerHTML = ''
            rowAuctionPage.prepend(cardOnAuction)
            auctionPage()
    }
}

// auction buttons
function auctionPage() {
    let bidsFromLs;
    userLoggedLS = localStorage.getItem('isArtist')


    let auctionWrap = document.createElement('div')
    auctionWrap.classList.add('col-12', )

    let timerDiv = document.createElement('div')
    timerDiv.classList.add('col-12', 'text-center')

    let timerSpan = document.createElement('span')
    timerSpan.classList.add('h4')
    timerSpan.innerText = 'Seconds left for bidding: '

    let timerDisplay = document.createElement('span')
    timerDisplay.classList.add('h3')
    timerDisplay.setAttribute('id', 'timer')
    timerDiv.append(timerSpan, timerDisplay)

    let biddingBox = document.createElement('div')
    biddingBox.classList.add('col-12', 'biddingBox', 'my-4')
    
    let bidInput = document.createElement('input')
    bidInput.classList.add('bidInputStyle')
    bidInput.setAttribute('type', 'number')
    bidInput.setAttribute('id', 'bidInput')
    bidInput.setAttribute('placeholder', 'Enter your bid...')

    let btnConfirmBid = document.createElement('button')
    btnConfirmBid.classList.add('btn', 'ml-5', 'bidBtnStyle')
    btnConfirmBid.setAttribute('id', 'btnConfirmBid')
    btnConfirmBid.innerText = 'Bid'
   

    biddingBox.append(bidInput, btnConfirmBid)

    let biddingLiveDisplay = document.createElement('div')
    biddingLiveDisplay.classList.add('col-12', 'liveBidding')

    let biddingUl = document.createElement('ul')
    biddingUl.classList.add('pl-0')
   
    if (userLoggedLS === 'true') {
        bidInput.disabled = true
        btnConfirmBid.disabled = true
        bidsFromLs = JSON.parse(localStorage.getItem('bidsInStorage')) 
        bidsFromLs.forEach(eachBid => {
            const li = document.createElement('li')
            li.classList.add('list-unstyled', 'mb-4', 'text-center')
            const spanArtistBid = document.createElement('span')
            spanArtistBid.classList.add('currentBid','px-4', 'py-2', 'rounded')
            spanArtistBid.innerText = `Current bid: $${eachBid}`
            li.appendChild(spanArtistBid)
            biddingUl.appendChild(li)
    
        })
    }

   

    biddingLiveDisplay.appendChild(biddingUl)
    
    auctionWrap.append(timerDiv, biddingBox, biddingLiveDisplay)

    let timerInterval
    let allBidsData = []

    function formatTime(seconds) {
        secondstToMinutes = seconds
        return secondstToMinutes
    }

    function initTimer(time = 120) {

        if (timerInterval) {
            clearInterval(timerInterval)
        }

        timerDisplay.innerText = time

        timerInterval = setInterval(() => {

            if (time === 0) {
                console.log('Auction Done!')
                clearInterval(timerInterval)
                currentBiddingItem.dateSold = new Date()
                currentBiddingItem.priceSold = allBidsData[allBidsData.length - 1]
                return
            }
            time--
            timerDisplay.innerText = formatTime(time)
        }, 1000)
    }
    function onBidHandler() {
        // apending my bids after click
        const li = document.createElement('li')
        li.classList.add( 'list-unstyled', 'mb-4')
        const mySpanBid = document.createElement('span')
        mySpanBid.innerText = `My bid is: $${bidInput.value}`
        mySpanBid.classList.add('my-bid', 'px-4', 'py-2')
        li.appendChild(mySpanBid)
        biddingUl.appendChild(li)
        allBidsData.push(bidInput.value)
        console.log(allBidsData)

        // making bid request
        makeBid(bidInput.value).then(data => {

            const { isBidding, bidAmount } = data

            // if is bidding == true
            if (isBidding) {
                initTimer(60)
                // appending their bid to the ul list
                const li = document.createElement('li')
                li.classList.add( 'list-unstyled', 'mb-4', 'text-right')
                const theirSpanBid = document.createElement('span')
                theirSpanBid.innerText = `Their bid is: $${bidInput.value}`
                theirSpanBid.classList.add('their-bid', 'px-4', 'py-2')
                li.appendChild(theirSpanBid)
                biddingUl.appendChild(li)

                allBidsData.push(bidAmount)
                console.log(allBidsData)
                localStorage.setItem('bidsInStorage', JSON.stringify(allBidsData))

                bidInput.setAttribute('min', bidAmount)
                bidInput.value = bidAmount + 10
            }
        })

    }
    initTimer()
    btnConfirmBid.addEventListener('click', onBidHandler)

    
    return auctionWrap
}

function makeBid(amount) {
    const url = 'https://blooming-sierra-28258.herokuapp.com/bid'
    const data = { amount }

    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        referrerPolicy: 'origin-when-cross-origin',
        body: JSON.stringify(data)
    }).then(res => res.json());
}

// auction back icon

function auctionBackIcon() {
    let backIcon = document.createElement('span')
    backIcon.classList.add('backIconStyle','pl-2', 'pt-2','position-absolute')
    backIcon.innerHTML = `<i class="fas fa-arrow-left fa-2x"></i>`
    backIcon.addEventListener('click', () => {
        location.hash = '#visitor'
    })
    rowAuctionPage.prepend(backIcon)

}
// camera
function initCaptureImagePage() {
    console.log('Capture image')

    // Initial widht and height of the video, will update later
    let width = 640;
    let height = 0;

    // flag for streaming or not (initial val is false)
    let currentStream;
    let currentStreamingIndex = 0;

    // Global vars in this init function
    let video = null;
    let canvas = null;
    let photo = null;
    let takeSnapShotBtn = null;
    let switchbutton = null;

    let allVideoDevices

    // The image
    let imageData;

    // Take all devices
    navigator.mediaDevices.enumerateDevices().then(data => {
        allVideoDevices = data.filter(device => device.kind === 'videoinput')
        switchbutton.removeAttribute('disabled')
    })

    // Get stream when switching camera
    function getStream() {
        currentStreamingIndex++
        const source = allVideoDevices[currentStreamingIndex % allVideoDevices.length].deviceId

        const constrains = {
            video: {
                deviceId: source ? { exact: source } : undefined
            }
        }

        navigator.mediaDevices.getUserMedia(constrains)
            .then((stream) => {
                currentStream = stream;
                video.srcObject = stream;
            })
    }


    function initCamera() {
        // selectors
        video = document.querySelector('#video');
        canvas = document.querySelector('#canvas');
        photo = document.querySelector('#photo');
        takeSnapShotBtn = document.querySelector('#startButton');
        switchbutton = document.querySelector('#switchButton');

        navigator.mediaDevices.getUserMedia({ video: true, audio: false })
            .then((stream) => {
                currentStream = stream
                video.srcObject = stream;
            })
            .catch(function (err) {
                console.log("An error occurred: " + err);
            });


        video.addEventListener('canplay', function (e) {

            height = video.videoHeight / (video.videoWidth / width);

            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);

        });


        takeSnapShotBtn.addEventListener('click', takepicture);


        switchbutton.addEventListener('click', function () {
            if (currentStream) {
                stopAllStream(currentStream)
                
            }
            
            getStream()
        })
    }

    function stopAllStream(stream) {
        stream.getTracks().forEach(track => {
            track.stop();
        });
    }

    function takepicture() {
        let context = canvas.getContext('2d');
        video.classList.remove('d-block')
        video.classList.add('d-none')
        cameraBtns.classList.add('d-none')
        takenPhoto.classList.remove('d-none')
        takenPhoto.classList.remove('d-block')
        outputImageCamera.classList.add('py-5')


        if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);
            imageData = canvas.toDataURL('image/png');
            photo.setAttribute('src', imageData);
        }
    }



    document.querySelector('#addNewItemBtn')
        .addEventListener('click', function () {
            const artName = currentArtist
            const newObjCapture = {
                "id": new Date().valueOf(),
                "title": addNewTitle.value,
                "description": addNewDescription.value,
                "type": addNewType.value,
                "image": imageData,
                "price": parseInt(addNewPrice.value),
                "artist": artName,
                "dateCreated": "2021-11-29T02:00:48.989Z",
                "isPublished": true,
                "isAuctioning": false,

            }

            items.push(newObjCapture)
            // location.hash = '#artists/items'
        


            snapShotDiv.classList.remove('d-flex')
            canvas.classList.remove('d-none')
            snapShotDiv.classList.add('d-none')
            // items.pop()

            location.hash ='#addNewItem'
            localStorage.setItem('itemsInLS', JSON.stringify(items))
        })

    initCamera()
}
